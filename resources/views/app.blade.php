<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="shortcut icon" type="image/png" href="/images/favicon.ico" />
    <title>e-BISYS Perumdam</title>
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
</head>

<body class="hold-transition layout-fixed">
    <div id="app"></div>
    <script src="{{ asset('js/app.js') }}"></script>
</body>

</html>