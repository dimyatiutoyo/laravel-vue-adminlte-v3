import axios from "axios";

export default {
  namespaced: true,

  state: {
    authenticated: false,
    user: null,
  },

  getters: {
    authenticated: (state) => {
      return state.authenticated;
    },

    user: (state) => {
      return state.user;
    },
  },

  mutations: {
    SET_AUTHENTICATED(state, value) {
      state.authenticated = value;
    },
    SET_USER(state, value) {
      state.user = value;
    },
  },

  actions: {
    async signIn({ dispatch }, credentials) {
      await axios.get("/sanctum/csrf-cookie");
      return await axios
        .post("/api/login", credentials)
        .then((response) => {
          return dispatch("me");
        })
        .catch(function(error) {
          dispatch("me");
          return error.response;
        });
    },

    async signOut({ dispatch }) {
      await axios.post("/api/logout");

      return dispatch("me");
    },

    me({ commit }) {
      return axios
        .get("/api/authenticateduser")
        .then((response) => {
          commit("SET_AUTHENTICATED", true);
          commit("SET_USER", response.data);
        })
        .catch(() => {
          commit("SET_AUTHENTICATED", false);
          commit("SET_USER", null);
        });
    },
  },
};
