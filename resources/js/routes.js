import Home from "./components/Home.vue";
import About from "./components/About.vue";
import Login from "./components/Login.vue";
import Menu from "./components/Menu.vue";

/**
 * Post Component
 */
import Post from "./components/post/Index.vue";
import PostCreate from "./components/post/Create.vue";

/**
 * Post Routing
 */
let postRoutes = [
  {
    name: "posts",
    path: "/posts",
    component: Post,
  },
  {
    name: "posts.create",
    path: "/posts/create",
    component: PostCreate,
  },
];

/**
 * All Routing
 */
export const routes = [
  {
    name: "",
    path: "",
    component: Login,
  },
  {
    name: "home",
    path: "/home",
    component: Home,
  },
  {
    name: "about",
    path: "/about",
    component: About,
  },
  {
    name: "menu",
    path: "/menu",
    component: Menu,
  },
  {
    name: "login",
    path: "/login",
    component: Login,
  },
  ...postRoutes,
];
