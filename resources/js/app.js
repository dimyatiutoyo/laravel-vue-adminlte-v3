/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require("./bootstrap");

window.Vue = require("vue");

// Import Vue-Router, Vue-Axios, Axios
import VueRouter from "vue-router";
import VueAxios from "vue-axios";
import Vuex from "vuex";
import Axios from "axios";
import VueSweetAlert2 from "vue-sweetalert2";
import "sweetalert2/dist/sweetalert2.min.css";
import { routes } from "./routes";
import auth from "./store/auth";
import App from "./components/App.vue";

Vue.use(VueRouter);
Vue.use(VueAxios, Axios);
Vue.use(VueSweetAlert2);
Vue.use(Vuex);

Vue.component("pagination", require("laravel-vue-pagination"));

const store = new Vuex.Store({
  modules: {
    auth,
  },
});

const router = new VueRouter({
  mode: "history",
  routes: routes,
});

store.dispatch("auth/me").then(() => {
  new Vue({
    el: "#app",
    router,
    store,
    render: (h) => h(App),
  });
});

// const app = new Vue({
//   el: "#app",
//   router,
//   store,
//   render: (h) => h(App),
// });
