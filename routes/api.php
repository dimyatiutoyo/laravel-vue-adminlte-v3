<?php

use App\Http\Controllers\LoginController;
use App\Http\Controllers\PostController;
use App\Http\Controllers\UserController;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::get('/setcookie', function () {
//     Cookie::queue('test', "my_shared_value", 10, null, '.perumdam.test');

//     return response('Hai');
// });

// Route::get('/getcookie', function () {
//     return Cookie::get('test');
// });


Route::post('/login', [LoginController::class, 'login']);
Route::post('/logout', [LoginController::class, 'logout']);

Route::get('/authenticateduser', function () {

    if (Auth::id() == null) {
        return response()->json(['message' => 'Unauthenticated'], 401);
    }
    $user = User::where(['id' => Auth::id()])->first();
    return response()->json($user);
});

Route::middleware('auth:sanctum')->group(function () {
    Route::resource('/user', UserController::class);
    Route::get('/posts', [PostController::class, 'index']);
    Route::post('/posts/store', [PostController::class, 'store']);
});
