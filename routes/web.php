<?php

use App\Http\Controllers\LoginController;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/setcookie', function () {
//     Cookie::queue('test', "my_shared_value", 10, null, '.perumdam.test');

//     return response('Hai');
// });

Route::get('/{any}', function () {
    return view('app');
})->where('any', '.*');
