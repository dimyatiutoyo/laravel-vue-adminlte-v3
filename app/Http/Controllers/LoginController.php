<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    public function index()
    {
        return view('auth.login');
    }

    public function login(Request $request)
    {
        $credentials = $request->only(['email', 'password']);

        if (Auth::attempt($credentials)) {
            $request->session()->regenerate();

            $user = User::where(['id' => Auth::id()])->first();

            return response()->json($user);
        }

        return response()->json([
            'message' => "Login failed. The email or password was incorrect. Try again."
        ], 401);
    }

    public function logout(Request $request)
    {
        try {
            return Auth::logout();

            $request->session()->invalidate();

            $request->session()->regenerateToken();
        } catch (\Throwable $th) {
            return response()->json('error_logout', 500);
        }
    }

    public function forgotPassword()
    {
        return view('auth.forgot-password');
    }
}
