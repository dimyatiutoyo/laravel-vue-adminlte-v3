<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

use Faker\Factory as Faker;

class PostSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create('id_ID');
        for ($i = 1; $i <= 50; $i++) {
            DB::table('posts')->insert([
                'user_id' => 1,
                'title' => $faker->sentence,
                'content' => $faker->paragraphs(4, true),
                'created_at' => now(),
                'updated_at' => now()
            ]);
        }
    }
}
