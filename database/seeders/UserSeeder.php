<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => "Fujiati Tirta",
            'email' => "fujiati.tirta@gmail.co.id",
            'email_verified_at' => now(),
            'level_id' => 1,
            'password' => Hash::make('12345'),
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        DB::table('users')->insert([
            'name' => "Ananda Saleha",
            'email' => 'ananda.saleha@gmail.com',
            'email_verified_at' => now(),
            'level_id' => 2,
            'password' => Hash::make('12345'),
            'created_at' => now(),
            'updated_at' => now(),
        ]);
    }
}
