<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMenusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('menus', function (Blueprint $table) {
            $table->id();
            $table->integer('parent_id');
            $table->integer('group_id');
            $table->enum('module', ['BILLING', 'SIAK', 'SIMPEG', 'SISTOR', 'SIMPRO']);
            $table->char('name');
            $table->text('description')->nullable();
            $table->char('route');
            $table->char('route_name');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('menus');
    }
}
